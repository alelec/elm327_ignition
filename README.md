# VAG MIB elm337 ignition
This script/app uses a cheap elm327 obd adapter to send CAN massages to a MIB unit to stop it going to sleep.

## Usage
```
elm327_ignition.exe COM3
```
Or
```
python3 elm327_ignition.py /dev/ttyUSB0
```

For more info see: https://gitlab.com/alelec/elm327_ignition/-/wikis/VAG-MIB-Test-Bench
