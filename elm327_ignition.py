########################################################
# Terminal status CAN bus message generator
# 
# Generates Klemmen_Status_01 messages for MQB platform
# 
# Author: Andrew Leech <andrew@alelec.net>
#
# Based on https://forums.ross-tech.com/index.php?threads/5918/post-203849
#  by Ronaldo Cordeiro - cordeiroronaldo@hotmail.com
# 

import sys
import time
import serial
import argparse
import itertools
from serial.tools import list_ports


def get_port():
    ports = list(list_ports.comports())
    if len(ports) == 1:
        return ports[0]


ELM_PROMPT = b'>'
ELM_LP_ACTIVE = b'OK'


def message(s: serial.Serial, data):
    s.write(data.encode())
    s.write(b"\r")
    s.flush()

    buffer = bytearray()
    while True:
        # retrieve as much data as possible
        data = s.read(s.in_waiting or 1)
        # if nothing was received
        if not data:
            # logger.warning("Failed to read port")
            break

        buffer.extend(data)

        # end on chevron (ELM prompt character) or an 'OK' which
        # indicates we are entering low power state
        if ELM_PROMPT in buffer or ELM_LP_ACTIVE in buffer:
            break
    return buffer.strip(ELM_PROMPT).decode().strip()


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('port', default=get_port(), nargs='?', help='elm327 com port')
    args = parser.parse_args()
    if not args.port:
        parser.print_help()
        sys.exit(-1)

    port = args.port

    s = serial.Serial(port=port, baudrate=38400)

    # Reset the elm
    message(s, "at z")
    # Mode 6: ISO 15765-4 (CAN 11/500)
    message(s, "at sp 6")
    # Display Headers Off
    message(s, "at h0")
    # Display Echo Off
    message(s, "at e0")

    # Send bus reset
    message(s, "at sh 000")
    msg = message(s, "00 00 00 00 00 00")
    msg = message(s, "00 00 00 00 00 00")
    msg = message(s, "00 00 00 00 00 00")
    # print(msg)

    # Set the correct header for ignition messages
    message(s, "at sh 3C0")

    # ignition_status = 0xF3
    ignition_status = 0x03

    spinner = itertools.cycle(['-', '/', '|', '\\'])
    last_rsp = 'NO DATA'
    while True:
        try:
            for i in range(256):
                ignition_status ^= 0xf0
                rsp = message(s, f"00 {ignition_status:02X} {i:02X}")
                # rsp = message(s, f"E6 02 27 00")
                # rsp = message(s, f"00 27 02 E6")
                if last_rsp != rsp:
                    print(rsp)
                    last_rsp = rsp
                time.sleep(0.1)

                sys.stdout.write(next(spinner))  # write the next character
                sys.stdout.flush()  # flush stdout buffer (actual character display)
                sys.stdout.write('\b')  # erase the last written char
        except KeyboardInterrupt:
            sys.exit(0)
        except:
            try:
                s.close()
                s.open()
            except:
                pass
            print(f"Connected: {s.is_open}")


if __name__ == "__main__":
    main()
